class AddVideoLinkToRefineryPages < ActiveRecord::Migration
  def change
    add_column :refinery_pages, :video_link, :text
  end
end
